# company hq location scraper

scrapes info about a company's hq from the [sec's edgar tool]().

## running

`npm start` to run all the scripts

## scripts

you can run each script with `npm run SCRIPTNAME`

* `scrape:symbols` - scrape all the russell 2000 company ticker symbols
* `scrape:locations` - scrape hq location from sec based on ticker symbol
