const fs = require('fs')

const writeJson = (path, j) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, JSON.stringify(j, null, 2), err => {
      if (err) return reject(err)

      resolve()
    })
  })
}

module.exports = {
  writeJson
}
