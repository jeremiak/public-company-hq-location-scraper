const http = require('axios')

const { writeJson } = require('./fs')

const url = 'https://core-api.barchart.com/v1/quotes/get?list=stocks.markets.russell2000&fields=symbol%2CsymbolName%2ClastPrice%2CpriceChange%2CpercentChange%2CopenPrice%2ChighPrice%2ClowPrice%2Cvolume%2CtradeTime%2CsymbolCode%2ChasOptions%2CsymbolType&orderBy=&orderDir=desc&meta=field.shortName%2Cfield.type%2Cfield.description&hasOptions=true&page=1&limit=2000&raw=1'

http.get(url).then(response => {
  return response.data.data.map(c => c.symbol)
}).then(symbols => {
  return symbols.map(symbol => ({ symbol }))
}).then(symbols => {
  writeJson('./symbols.json', symbols)
})
