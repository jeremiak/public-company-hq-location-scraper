const cheerio = require('cheerio')
const http = require('axios')
const Queue = require('queue')

const symbols = require('./symbols.json')
const { writeJson } = require('./fs')

const queue = Queue({
  concurrency: 4
})
const errors = []
const results = []

const getSECUrl = tickerSymbol => (
  `https://www.sec.gov/cgi-bin/browse-edgar?CIK=${tickerSymbol}&owner=exclude&action=getcompany`
)

const getCompanyLocation = tickerSymbol => {
  const url = getSECUrl(tickerSymbol)
  return http.get(url).then(response => {
    const $ = cheerio.load(response.data)
    const info = $('.identInfo').text()
    const sic = info.match(/SIC:\s(\d+)/)
    const businessAddress = $('.mailer').slice(1, 2).text().split('\n').map(s => s.trim())
    const businessState = info.match(/State location:\s(\w{2})/)
    const incorporationState = info.match(/State of Inc.:\s(\w{2})/)

    return {
      sic: sic && sic[1],
      symbol: tickerSymbol,
      businessAddress: businessAddress && businessAddress.slice(businessAddress.length - 3, businessAddress.length - 2).pop(),
      businessState: businessState && businessState[1],
      incorporationState: incorporationState && incorporationState[1]
    }
  })
}

queue.on('end', err => {
  if (err) return console.error('end but err', err)

  console.log(`processed ${symbols.legth} resulting in ${results.length} successes and ${errors.length} errors`)
  writeJson('./location-errors.json', errors)
  writeJson('./location-results.json', results)
})

symbols.forEach(company => {
  queue.push(() => {
    return getCompanyLocation(company.symbol).then(d => {
      results.push(d)
    }).catch(() => errors.push(company))
  })
})

queue.start()
